﻿using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;

namespace Hiroyuki.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private DelegateCommand _logoutCommand;
        private LoginWindowViewModel _loginWindowViewModel;

        public LoginWindowViewModel LoginWindowViewModel
        {
            get { return _loginWindowViewModel; }
            set
            {
                _loginWindowViewModel = value;
                OnPropertyChanged();
            }
        }

        public MainWindowViewModel()
        {
            LoginWindowViewModel = new LoginWindowViewModel();
        }

        public ICommand LogoutCommand
        {
            get { return _logoutCommand ?? (_logoutCommand = new DelegateCommand(OnExecuteLogoutCommand)); }
        }

        private void OnExecuteLogoutCommand()
        {
            LoginWindowViewModel.Logout();
        }


    }
}