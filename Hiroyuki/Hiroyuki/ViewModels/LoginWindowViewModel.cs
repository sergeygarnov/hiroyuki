﻿using System.Windows.Input;
using Hiroyuki.Views;
using Prism.Commands;
using Prism.Mvvm;
using static System.String;

namespace Hiroyuki.ViewModels
{
    public class LoginWindowViewModel : BindableBase
    {
        private string _login;
        private string _password;
        private DelegateCommand _loginCommand;
        private bool _successLogin;

        private const string ValidLogin = "hiroyuki";
        private const string ValidPassword = "hiroyuki";

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged();
                _loginCommand.RaiseCanExecuteChanged();
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value; 
                OnPropertyChanged();
                _loginCommand.RaiseCanExecuteChanged();
            }
        }

        public bool SuccessLogin
        {
            get { return _successLogin; }
            set
            {
                _successLogin = value;
                OnPropertyChanged();
            }
        }

        public ICommand LoginCommand
        {
            get
            {
                return _loginCommand ??
                       (_loginCommand = new DelegateCommand(OnExecuteLoginCommand, CanExecuteLoginCommand));
            }
        }

        private bool CanExecuteLoginCommand()
        {
            return !IsNullOrEmpty(Login) && !IsNullOrEmpty(Password);
        }

        private void OnExecuteLoginCommand()
        {
            SuccessLogin = ValidateUser(Login, Password);
        }

        private bool ValidateUser(string login, string password)
        {
            return login == ValidLogin && password == ValidPassword;
        }

        public void Logout()
        {
            SuccessLogin = false;
            Password = Empty;
            Login = Empty;
        }
    }
}