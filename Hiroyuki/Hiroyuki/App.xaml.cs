﻿using System.Windows;
using Hiroyuki.ViewModels;
using Hiroyuki.Views;

namespace Hiroyuki
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var view = new MainWindow {DataContext = new MainWindowViewModel()};
            view.Show();
            base.OnStartup(e);
        }
    }
}
